<?php

declare(strict_types=1);

namespace Whatsrabbit\LiveChatPluginCoreTest\ValueObject;

use DateTimeImmutable;
use PHPUnit\Framework\TestCase;
use Whatsrabbit\LiveChatPluginCore\Exception\AuthenticationResponseException;
use Whatsrabbit\LiveChatPluginCore\ValueObject\AuthenticationResponse;

class AuthenticationResponseTest extends TestCase
{
    public function testCreateFromArray(): void
    {
        $date = new DateTimeImmutable();
        $response = AuthenticationResponse::createFromArray([
            'externalId' => 'some-external-id',
            'token' => 'some-token',
            'refreshToken' => 'some-refresh-token',
            'refreshTokenExpiresAt' => $date->format(DATE_ATOM),
        ]);

        $this->assertInstanceOf(AuthenticationResponse::class, $response);
        $this->assertSame('some-external-id', $response->getExternalId());
        $this->assertSame('some-token', $response->getToken());
        $this->assertSame('some-refresh-token', $response->getRefreshToken());
        $this->assertSame($date->format(DATE_ATOM), $response->getRefreshTokenExpiresAt()->format(DATE_ATOM));
    }

    public function testCreateFromArrayFailsWhenMissingKey(): void
    {
        $date = new DateTimeImmutable();

        $this->expectException(AuthenticationResponseException::class);
        $this->expectExceptionMessage('Unable to create AuthenticationResponse. The "externalId" key is missing inside the data.');

        $response = AuthenticationResponse::createFromArray([
            'token' => 'some-token',
            'refreshToken' => 'some-refresh-token',
            'refreshTokenExpiresAt' => $date->format(DATE_ATOM),
        ]);
    }

    public function testCreateFromArrayFailsWhenEmptyKey(): void
    {
        $date = new DateTimeImmutable();

        $this->expectException(AuthenticationResponseException::class);
        $this->expectExceptionMessage('Unable to create AuthenticationResponse. No value provided for property "externalId".');

        $response = AuthenticationResponse::createFromArray([
            'externalId' => '',
            'token' => 'some-token',
            'refreshToken' => 'some-refresh-token',
            'refreshTokenExpiresAt' => $date->format(DATE_ATOM),
        ]);
    }

    public function testCreateFromArrayFailsWhenInvalidDate(): void
    {
        $date = new DateTimeImmutable();

        $this->expectException(AuthenticationResponseException::class);
        $this->expectExceptionMessage('Unable to create DateTimeImmutable');

        $response = AuthenticationResponse::createFromArray([
            'externalId' => 'some-external-id',
            'token' => 'some-token',
            'refreshToken' => 'some-refresh-token',
            'refreshTokenExpiresAt' => $date,
        ]);
    }
}
