<?php

declare(strict_types=1);

namespace Whatsrabbit\LiveChatPluginCoreTest;

use DateTimeImmutable;
use Exception;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use Mockery\Mock;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use TestTools\AutoFixture;
use Whatsrabbit\LiveChatPluginCore\Exception\LiveChatException;
use Whatsrabbit\LiveChatPluginCore\LiveChatService;

class LiveChatServiceTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    public ClientInterface|Mock $httpClient;

    protected function setUp(): void
    {
        parent::setUp();

        $this->httpClient = Mockery::mock(ClientInterface::class);
    }

    public function testCanCreate(): void
    {
        $instance = new LiveChatService(
            apiKey: '',
            apiSecret: '',
            httpClient: $this->httpClient,
        );

        $this->assertInstanceOf(LiveChatService::class, $instance);
    }

    public function testItCanFetchToken(): void
    {
        // Given
        $service = new LiveChatService(
            apiKey: '',
            apiSecret: '',
            httpClient: $this->httpClient,
        );

        $givenExternalId = AutoFixture::string();
        $givenToken = AutoFixture::string();
        $givenRefreshToken = AutoFixture::string();
        $givenRefreshTokenExpiresAt = (new DateTimeImmutable())
            ->setTimestamp(AutoFixture::number(3600 * 24 * 365 * 30));

        $mockResponseBody = Mockery::mock(StreamInterface::class);
        $mockResponseBody->expects('getContents')->andReturn(json_encode([
            'data' => [
                'externalId' => $givenExternalId,
                'token' => $givenToken,
                'refreshToken' => $givenRefreshToken,
                'refreshTokenExpiresAt' => $givenRefreshTokenExpiresAt->format('c'),
            ],
        ], JSON_THROW_ON_ERROR));

        $mockResponse = Mockery::mock(ResponseInterface::class);
        $mockResponse->expects('getStatusCode')->andReturn(200);
        $mockResponse->expects('getBody')->andReturn($mockResponseBody);

        $this->httpClient->expects('sendRequest')->andReturn($mockResponse);

        // When
        $result = $service->fetchToken();

        // Then

        $jsonResponse = $result->jsonSerialize();
        $this->assertArrayHasKey('externalId', $jsonResponse);
        $this->assertSame($givenExternalId, $jsonResponse['externalId'], 'ExternalId does not match');
        $this->assertArrayHasKey('token', $jsonResponse);
        $this->assertSame($givenToken, $jsonResponse['token'], 'Token does not match');
        $this->assertArrayHasKey('refreshToken', $jsonResponse);
        $this->assertSame($givenRefreshToken, $jsonResponse['refreshToken'], 'RefreshToken does not match');
        $this->assertArrayHasKey('refreshTokenExpiresAt', $jsonResponse);
        $this->assertEquals($givenRefreshTokenExpiresAt, $jsonResponse['refreshTokenExpiresAt'], 'RefreshTokenExpiresAt does not match');
    }

    public function testItThrowsAnExceptionOnFetchTokenWhenClientExceptionInterfaceWasThrown(): void
    {
        // Given
        $service = new LiveChatService(
            apiKey: '',
            apiSecret: '',
            httpClient: $this->httpClient,
        );

        $this->httpClient->expects('sendRequest')->andThrows(Exception::class);

        // Then
        $this->expectException(LiveChatException::class);
        $this->expectExceptionMessage('Failed to make request');

        // When
        $service->fetchToken();
    }

    public function testItThrowsAnExceptionOnFetchTokenWhenHttpErrorStatusCodeIsReceived(): void
    {
        // Given
        $service = new LiveChatService(
            apiKey: '',
            apiSecret: '',
            httpClient: $this->httpClient,
        );

        $mockResponse = Mockery::mock(ResponseInterface::class);
        $mockResponse->expects('getStatusCode')->twice()->andReturn(418);

        $this->httpClient->expects('sendRequest')->andReturn($mockResponse);

        // Then
        $this->expectException(LiveChatException::class);
        $this->expectExceptionMessage('Got a bad response, received a 418 HTTP status code.');

        // When
        $service->fetchToken();
    }

    public function testItThrowsAnExceptionOnFetchTokenWhenAnEmptyBodyIsReceived(): void
    {
        // Given
        $service = new LiveChatService(
            apiKey: '',
            apiSecret: '',
            httpClient: $this->httpClient,
        );

        $mockResponseBody = Mockery::mock(StreamInterface::class);
        $mockResponseBody->expects('getContents')->andReturn(json_encode([]));

        $mockResponse = Mockery::mock(ResponseInterface::class);
        $mockResponse->expects('getStatusCode')->andReturn(200);
        $mockResponse->expects('getBody')->andReturn($mockResponseBody);

        $this->httpClient->expects('sendRequest')->andReturn($mockResponse);

        // Then
        $this->expectException(LiveChatException::class);
        $this->expectExceptionMessage('Received a response, but no data received.');

        // When
        $service->fetchToken();
    }

    public function testItThrowsAnExceptionOnFetchTokenWhenAnDataIsMissingInsideTheResponseBody(): void
    {
        // Given
        $service = new LiveChatService(
            apiKey: '',
            apiSecret: '',
            httpClient: $this->httpClient,
        );

        $mockResponseBody = Mockery::mock(StreamInterface::class);
        $mockResponseBody->expects('getContents')->andReturn(json_encode([
            'data' => [],
        ], JSON_THROW_ON_ERROR));

        $mockResponse = Mockery::mock(ResponseInterface::class);
        $mockResponse->expects('getStatusCode')->andReturn(200);
        $mockResponse->expects('getBody')->andReturn($mockResponseBody);

        $this->httpClient->expects('sendRequest')->andReturn($mockResponse);

        // Then
        $this->expectException(LiveChatException::class);
        $this->expectExceptionMessage('Unable to create AuthenticationResponse object.');

        // When
        $service->fetchToken();
    }

    public function testItThrowsAnExceptionOnFetchTokenWhenAnDataIsMissingOnThePropertiesTheResponseBody(): void
    {
        // Given
        $service = new LiveChatService(
            apiKey: '',
            apiSecret: '',
            httpClient: $this->httpClient,
        );

        $mockResponseBody = Mockery::mock(StreamInterface::class);
        $mockResponseBody->expects('getContents')->andReturn(json_encode([
            'data' => [
                'token' => '',
                'refreshToken' => '',
                'refreshTokenExpiresAt' => '',
            ],
        ], JSON_THROW_ON_ERROR));

        $mockResponse = Mockery::mock(ResponseInterface::class);
        $mockResponse->expects('getStatusCode')->andReturn(200);
        $mockResponse->expects('getBody')->andReturn($mockResponseBody);

        $this->httpClient->expects('sendRequest')->andReturn($mockResponse);

        // Then
        $this->expectException(LiveChatException::class);
        $this->expectExceptionMessage('Unable to create AuthenticationResponse object.');

        // When
        $service->fetchToken();
    }

    public function testItThrowsAnExceptionOnFetchTokenWhenInvalidJson(): void
    {
        // Given
        $service = new LiveChatService(
            apiKey: '',
            apiSecret: '',
            httpClient: $this->httpClient,
        );

        $mockResponseBody = Mockery::mock(StreamInterface::class);
        $mockResponseBody->expects('getContents')->andReturn('{data: null');

        $mockResponse = Mockery::mock(ResponseInterface::class);
        $mockResponse->expects('getStatusCode')->andReturn(200);
        $mockResponse->expects('getBody')->andReturn($mockResponseBody);

        $this->httpClient->expects('sendRequest')->andReturn($mockResponse);

        // Then
        $this->expectException(LiveChatException::class);
        $this->expectExceptionMessage('Failed to decode message');

        // When
        $service->fetchToken();
    }
}
