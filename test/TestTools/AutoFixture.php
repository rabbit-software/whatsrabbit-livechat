<?php

declare(strict_types=1);

namespace TestTools;

class AutoFixture
{
    public static function number(int $max = 100, int $min = 1): int
    {
        return random_int($min, $max);
    }

    public static function float(int $max = 10, int $min = 1): float
    {
        return random_int($min, $max) + random_int(0, 10000) / 10000;
    }

    public static function string(int $length = 5): string
    {
        $result = '';

        $characters = str_split('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789');
        $charactersLength = count($characters);

        for ($i = 0; $i < $length; $i++) {
            $result .= $characters[random_int(0, $charactersLength - 1)];
        }

        return $result;
    }

    public static function boolean(): bool
    {
        return self::pickOne(true, false);
    }

    public static function pickOne(mixed ...$items): mixed
    {
        $length = count($items);

        $pick = random_int(0, $length - 1);

        return $items[$pick];
    }
}
